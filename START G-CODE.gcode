G21
G90
M82
M107

G28 X0 Y0 ;home sur X & Y
G28 Z0 ; home Z
G1 Z1.0 F3000 ; Déplace Z de 1mm pour éviter de frotter


G92 E0

G1 X10 Y100 Z0.25 F5000.0
G1 X10 Y20 Z0.25 F1500.0 E20 ; Trace une ligne


G92 E0
G1 F{travel_xy_speed}

M117 Printing...